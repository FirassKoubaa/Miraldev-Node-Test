var express = require("express");
var app     = express();
var path    = require("path");
var fs = require('fs');
var Converter = require("csvtojson").Converter;

var jsonData;
// Headers
app.use(function (req, res, next) {
    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:4200');
    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    // Set to true if you need the website to include cookies in the requests sent
    res.setHeader('Access-Control-Allow-Credentials', true);
    // Pass to next layer of middleware
    next();
});

app.get('/',function(req,res){
    res.sendFile(path.join(__dirname+'/View/index.html'));
});

// create a new converter object
var converter = new Converter({});
// call the fromFile function which takes in the path to your csv file as well as a callback function
converter.fromFile("./data.csv",function(err,result){
    // if an error has occured then handle it
    if(err){
        console.log("An Error Has Occured");
        console.log(err);
    }
    // create a variable called json and store the result of the conversion
     jsonData = result;
    // log our json to verify it has worked
    console.log(jsonData);
});

app.get("/data",function(req,res){
    res.json(jsonData);
});

app.listen(3000);
console.log("Running at Port 3000");